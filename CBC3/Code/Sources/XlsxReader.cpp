#include <XlsxReader.hpp>

#include <iostream>
#include <cassert>
#include <stdio.h>
#include <string.h>


XlsxReader::XlsxReader(std::string path)
    : path(path)
{
    this->document = std::make_shared<XLDocument>();
    this->worksheet_name = this->getNameFromPath(path);

    std::cout << "File name is: " << this->worksheet_name << std::endl;

    try
    {
        this->document->OpenDocument(this->worksheet_name);
    }

    catch (std::exception &e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return;
    }

}


std::string XlsxReader::getNameFromPath(std::string path)
{
    int slash_position = 0;
    for (int i = path.length() ; i > -1; --i)
    {
        std::cout << path[i] << std::endl;
        if (path[i] == '/')
        {
            slash_position = i + 1;
            break;
        }
    }
    return path.substr(slash_position, path.length());
}


std::string XlsxReader::readCell(uint32_t row, uint32_t column)
{
    std::string first_ws_id = this->document->Workbook().WorksheetNames()[0];
    auto wsheet = this->document->Workbook().Worksheet(first_ws_id);
    return wsheet.Cell(row, column).Value().AsString();
}
