#include <Bc3Writer.hpp>

#include <cassert>
#include <iostream>
#include <fstream>

#include <Bc3Certification.hpp>
#include <Bc3Text.hpp>
#include <Bc3CompElement.hpp>
#include <Bc3CompChild.hpp>

#include <QDate>


namespace Bc3Types
{
    Bc3Writer::Bc3Writer(std::string path, bool create, bool append)
        : FileManip(path, create), append(append)
    {
        this->openFile();
    }


    Bc3Writer::~Bc3Writer()
    {

    }


    void Bc3Writer::addString(std::string str)
    {
        this->file << str;
    }


    void Bc3Writer::addLine(std::string line)
    {
        this->file << std::string(line + '\n');
    }


    void Bc3Writer::openFile()
    {
        if (!this->createFile() && this->append)
        {
            this->file = std::ofstream(this->getPath(), std::ios::in | std::ios::app);
        }

        else if (this->createFile() && this->append)
        {
            this->file = std::ofstream(this->getPath(), std::ios::app);
        }

        else
        {
            this->file = std::ofstream(this->getPath());
        }


        if (!this->file.good())
        {
            assert(this->file.good() && "Can't open file.");
        }
    }


    void Bc3Writer::addCertification(std::string code, bool is_root, std::string unit,
                                     std::string description, float price, QDate date)
    {
        std::string pre = "~C|";
        std::string cert_code;

        //Building code for certification.
        if (is_root)
            cert_code = code + "##|";
        else
            cert_code = code + "#|";

        //Building measurement unit.
        std::string m_unit = "|";


        //Building description. (Already built in parameter)
        description = description + "|";


        //Building price.
        std::string pr_str = this->buildPrice(price) + "|";


        //Building date.
        std::string str_date = this->generateDate(date) + "|";

        std::string final = pre + cert_code + m_unit + description + pr_str + str_date;

        std::cout << "Final string is: " << final << std::endl;

        this->addLine(final);
    }


    void Bc3Writer::addCertification(Bc3Certification *certification)
    {
        std::string pre = "~C|";
        std::string cert_code;

        if (certification->isRoot())
            cert_code = certification->getCode() + "##|";
        else
            cert_code = certification->getCode() + "#|";

        std::string final = pre + cert_code;

        this->addLine(final);
    }


    void Bc3Writer::addText(std::string code, std::string descriptive_text)
    {
        std::string pre = "~T|";
        std::string final = pre + code + "|" +  descriptive_text + "|";

        this->addLine(final);
    }


    void Bc3Writer::addText(Bc3Text *text)
    {
        std::string pre = "~T|";
        std::string final = pre + "|" + text->getDescribedCode() + "|" + text->getDescriptiveText() + "|";

        this->addLine(final);
    }


    void Bc3Writer::addCompElement(std::string code, std::vector<Bc3CompElement> comps)
    {
        std::string pre = "~D|";
        std::string all_comps;

        for (auto & c : comps)
        {
            pre +=c.getParentCode() + "|";
            for (auto & cc : c.getCompChildren())
            {
                pre += cc.getCode() + '\\' + std::to_string(cc.getFactor()) + '\\' + std::to_string(cc.getPerformance()) + "\\|";
            }
        }

        this->addLine(pre);

    }


    std::string Bc3Writer::buildPrice(float price)
    {
        std::string temp_price = std::to_string(price);
        for (int i = temp_price.length(); i > -1; --i)
        {
            std::cout << temp_price[i] << std::endl;
            if (temp_price[i] == ',')
                temp_price[i] = '.';
        }

        return temp_price;
    }


    std::string Bc3Writer::generateDate(QDate &date)
    {
        std::string day, month, year;
        day = date.day();
        if (day.length() < 2)
            day = "0" + std::to_string(date.day());
        std::cout << "Day: " << day;

        month = date.month();
        if (month.length() < 2)
            month = "0" + std::to_string(date.month());

        year = std::to_string(date.year());

        std::string f= day + month + year;
        return f;
    }

}
