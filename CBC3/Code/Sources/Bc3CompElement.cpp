#include <Bc3CompElement.hpp>

#include <Bc3CompChild.hpp>


namespace Bc3Types
{

    Bc3CompElement::Bc3CompElement(std::string parent_code, std::vector<Bc3CompChild> comp_children)
        : parent_code(parent_code), comp_children(comp_children)
    {

    }


    void Bc3CompElement::setParentCode(std::string parent_code)
    {
        this->parent_code = parent_code;
    }


    void Bc3CompElement::setCompChildren(std::vector<Bc3CompChild> &elements)
    {
        this->comp_children.reserve(elements.size());
        for (auto & e : elements)
        {
            this->comp_children.push_back(e);
        }
    }


    std::string Bc3CompElement::getParentCode()
    {
        return this->parent_code;
    }


    std::vector<Bc3CompChild> &Bc3CompElement::getCompChildren()
    {
        return this->comp_children;
    }


}
