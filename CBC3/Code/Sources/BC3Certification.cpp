#include <Bc3Certification.hpp>


namespace Bc3Types
{
    Bc3Certification::Bc3Certification(std::string &code)
        : code(code)
    {

    }


    std::string &Bc3Certification::getCode()
    {
        return this->code;
    }


    void Bc3Certification::isRoot(bool is_root)
    {
        this->is_root = is_root;
    }


    bool Bc3Certification::isRoot()
    {
        return this->is_root;
    }


    void Bc3Certification::setShortDescription(std::string short_description)
    {
        this->short_description = short_description;
    }


    std::string &Bc3Certification::getShortDescription()
    {
        return this->short_description;
    }


    void Bc3Certification::setPrice(float price)
    {
        this->price = price;
    }


    float Bc3Certification::getPrice()
    {
        return this->price;
    }


    void Bc3Certification::setLastUpdate(QDate &last_update)
    {
        this->last_update = last_update;
    }


    QDate &Bc3Certification::getLastDateUpdate()
    {
        return this->last_update;
    }


    void Bc3Certification::setType(int type)
    {
        this->type = intToBc3Type(type);
    }


    void Bc3Certification::setType(Bc3Types::Bc3Unit type)
    {
        this->type = type;
    }


    Bc3Unit Bc3Certification::getType()
    {
        return this->type;
    }


}
