#include <Bc3CompChild.hpp>


namespace Bc3Types
{

    Bc3CompChild::Bc3CompChild(std::string code, float factor, float performance)
        : code(code), factor (factor), performance(performance)
    {

    }


    void Bc3CompChild::setCode(std::string code)
    {
        this->code = code;
    }


    void Bc3CompChild::setFactor(float factor)
    {
        this->factor = factor;
    }


    void Bc3CompChild::setPerformance(float performance)
    {
        this->performance = performance;
    }


    std::string Bc3CompChild::getCode()
    {
        return this->code;
    }


    float Bc3CompChild::getFactor()
    {
        return this->factor;
    }


    float Bc3CompChild::getPerformance()
    {
        return this->performance;
    }

}
