#include <FileManip.hpp>


FileManip::FileManip(std::string path, bool create)
    : path (path), create_new_file(create)
{

}


bool FileManip::createFile()
{
    return this->create_new_file;
}


std::string &FileManip::getPath()
{
    return this->path;
}
