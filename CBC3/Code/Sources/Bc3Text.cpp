#include <Bc3Text.hpp>


namespace Bc3Types
{

    Bc3Text::Bc3Text(std::string described_code, std::string descriptive_text)
        : described_code (described_code), descriptive_text(descriptive_text)
    {

    }


    void Bc3Text::setDescribedCode(std::string code)
    {
        this->described_code = code;
    }


    void Bc3Text::setDescriptiveText(std::string descriptive_text)
    {
        this->descriptive_text = descriptive_text;
    }


    std::string &Bc3Text::getDescribedCode()
    {
        return this->described_code;
    }


    std::string &Bc3Text::getDescriptiveText()
    {
        return this->descriptive_text;
    }


}
