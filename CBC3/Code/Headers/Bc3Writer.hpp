#ifndef BC3WRITER_HPP
#define BC3WRITER_HPP

#include <FileManip.hpp>

#include <fstream>
#include <vector>

#include <QDate>



namespace Bc3Types
{
    class Bc3Certification;
    class Bc3Text;
    class Bc3CompElement;

    class Bc3Writer : public FileManip
    {
    public:
        Bc3Writer(std::string path, bool create_file = false, bool append = true);
        virtual ~Bc3Writer();

        void addString(std::string str);
        void addLine(std::string line);
        void addCertification(std::string code, bool is_root, std::string unit = "",
                              std::string description = "No description", float price = 0.00f,
                              QDate date = QDate());
        void addCertification(Bc3Certification *certification);
        void addText(std::string code, std::string descriptive_text);
        void addText(Bc3Text *text);
        void addCompElement(std::string code, std::vector <Bc3CompElement> comps);


    private:
        bool append = true;

        std::ofstream file;

        void openFile() override;

        std::string buildPrice(float price);
        std::string generateDate(QDate &date);


    };

}



#endif // BC3WRITER_HPP
