#ifndef BC3COMPCHILD_HPP
#define BC3COMPCHILD_HPP

#include <string>


namespace Bc3Types
{

    class Bc3CompChild
    {
    public:
        Bc3CompChild(std::string code, float factor, float performance);

        void setCode(std::string code);
        void setFactor(float factor);
        void setPerformance(float performance);

        std::string getCode();

        float getFactor();
        float getPerformance();


    private:
        std::string code;

        float factor;
        float performance;


    };

}


#endif // BC3COMPCHILD_HPP
