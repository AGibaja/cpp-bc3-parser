#ifndef BC3CERTIFICATION_HPP
#define BC3CERTIFICATION_HPP

#include <string>

#include <QDate>

#include <Bc3Types.hpp>


namespace Bc3Types
{
    class Bc3Certification
    {
    public:
        Bc3Certification(std::string &code);

        std::string &getCode();

        void isRoot(bool is_root);
        bool isRoot();

        void setShortDescription(std::string short_description);
        std::string &getShortDescription();

        void setPrice(float price);
        float getPrice();

        void setLastUpdate(QDate &last_update);
        QDate &getLastDateUpdate();

        void setType(int type);
        void setType(Bc3Types::Bc3Unit type);

        Bc3Unit getType();


    private:
        std::string code = "000000";

        bool is_root = false;

        std::string short_description = "";

        float price = 0.00f;

        QDate last_update;

        Bc3Unit type = Bc3Unit::UNDEFINED;


    };

}

#endif // BC3CERTIFICATION_HPP
