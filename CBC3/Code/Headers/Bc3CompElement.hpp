#ifndef BC3COMPELEMENT_HPP
#define BC3COMPELEMENT_HPP

#include <string>
#include <vector>


namespace Bc3Types
{
    class Bc3CompChild;

    class Bc3CompElement
    {
    public:
        Bc3CompElement(std::string parent_code, std::vector<Bc3CompChild> comp_children);

        void setParentCode(std::string parent_code);
        void setCompChildren(std::vector<Bc3CompChild> &elements);

        std::string getParentCode();
        std::vector <Bc3CompChild> &getCompChildren();


    private:
        std::string parent_code;

        std::vector<Bc3CompChild> comp_children;


    };
}


#endif // BC3COMPELEMENT_HPP
