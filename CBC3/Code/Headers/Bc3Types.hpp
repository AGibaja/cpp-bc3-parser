#ifndef BC3TYPES_HPP
#define BC3TYPES_HPP

#include <type_traits>

namespace Bc3Types
{
    enum Bc3Unit
    {
        UNDEFINED = 0,
        LABOUR,
        MACHINERY,
        MATERIALS,
        ADD_RES_COMP,
        RESIDUAL_TYPE
    };


    static int bc3TypeToInt(Bc3Unit unit)
    {
       switch (unit)
       {
           case(UNDEFINED):
           {
               return 0;
           }

           case(LABOUR):
           {
               return 1;
           }

           case(MACHINERY):
           {
               return 2;
           }

           case(MATERIALS):
           {
               return 3;
           }

           case(ADD_RES_COMP):
           {
               return 4;
           }

           case(RESIDUAL_TYPE):
           {
               return 5;
           }

           default:
           {
                return 0;
           }


       }
    }


    static Bc3Unit intToBc3Type(int unit)
    {
        switch (unit)
        {
        case (0):
            return Bc3Unit::UNDEFINED;

        case (1):
            return Bc3Unit::LABOUR;

        case (2):
            return Bc3Unit::MACHINERY;

        case (3):
            return Bc3Unit::ADD_RES_COMP;

        case (4):
            return Bc3Unit::RESIDUAL_TYPE;

        default:
            return Bc3Unit::UNDEFINED;
        }

    }


}


#endif // BC3TYPES_HPP
