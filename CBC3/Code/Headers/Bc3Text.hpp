#ifndef BC3TEXT_HPP
#define BC3TEXT_HPP

#include <string>


namespace Bc3Types
{
    class Bc3Text
    {
        public:
            Bc3Text(std::string code, std::string descriptive_text);

            void setDescribedCode(std::string code);
            void setDescriptiveText(std::string descriptive_text);

            std::string &getDescribedCode();
            std::string &getDescriptiveText();


        private:
            std::string described_code = "";
            std::string descriptive_text = "";

    };

}

#endif // BC3TEXT_HPP
