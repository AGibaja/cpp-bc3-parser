#ifndef FILEMANIP_HPP
#define FILEMANIP_HPP

#include <string>
#include <fstream>


class FileManip
{
public:
    FileManip(std::string path, bool create = false);

    virtual void openFile() = 0;

    bool createFile();

    std::string &getPath();


private:
    bool create_new_file = false;

    std::string path;
    std::string file_name;


};

#endif // FILEMANIP_HPP
