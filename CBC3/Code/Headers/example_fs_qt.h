#ifndef EXAMPLE_FS_QT_H
#define EXAMPLE_FS_QT_H

#include <QDir>
#include <QFile>
#include <QDebug>



class FSTest
{
public:


private:
    FSTest()
    {
        QDir dir ("/run/user/1000/gvfs/smb-share:server=192.168.1.6,share=datos/TEPROELEC");
        QFile file (dir.path() + "/test.txt");

        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            qDebug() << "File doesn't exist";
        }

        else
        {
            qDebug() << "File exists. Contents: " << file.readAll();
        }

        qDebug() << "Files in directory: ";

        for (auto & f : dir.entryList())
        {
            qDebug() << f;
        }
    }


};




#endif // EXAMPLE_FS_QT_H
