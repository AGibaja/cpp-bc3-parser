#ifndef XLSXREADER_HPP
#define XLSXREADER_HPP

#include <string>
#include <memory>

#include <OpenXLSX/OpenXLSX.h>

using namespace OpenXLSX;

class XlsxReader
{
public:
    XlsxReader(std::string path);

    std::string readCell(uint32_t row, uint32_t column);


private:
    std::string path = "Undefined path";
    std::string worksheet_name = "Unnamed worksheet";

    std::shared_ptr<OpenXLSX::XLDocument> document = nullptr;


    std::string getNameFromPath(std::string path);



};

#endif // XLSXREADER_HPP
