#ifndef BC3VERSION_HPP
#define BC3VERSION_HPP

#include <QDate>

#include <string>


namespace Bc3Types
{
    class Bc3Version
    {
    public:
        Bc3Version();



    private:
        std::string owner = "None";
        std::string version = "0.0.0.0";

        QDate creation_date;

        std::string emitter_program = "CBC3";
        std::string header = "No Header";
        std::string rotule = "No Rotule";
        std::string charset = "ASCII";
        std::string comment = "No Comment";
        std::string info_type = "NaN";
        std::string cert_number = "0.0.0.0";

        QDate cert_date;

        std::string base_url = "No base url";

    };
}

#endif // BC3VERSION_HPP
